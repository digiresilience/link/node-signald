import { SignaldGeneratedApi, JsonMessageEnvelopev1 } from "./generated";

export class SignaldAPI extends SignaldGeneratedApi {
  constructor() {
    super();
  }

  public async subscribev0(account: string): Promise<void> {
    return this.getResponse({
      type: "subscribe",
      username: account,
    }) as Promise<void>;
  }

  public async unsubscribev0(account: string): Promise<void> {
    return this.getResponse({
      type: "unsubscribe",
      username: account,
    }) as Promise<void>;
  }
}
